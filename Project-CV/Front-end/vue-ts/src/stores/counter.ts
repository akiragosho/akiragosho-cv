import { defineStore } from 'pinia'
import api from '@/services/axios.service'
interface IState {
  count: number
}

const initState: IState = {
  count: 0
}

export const useCounterStore = defineStore('counter', {
  state: (): IState => {
    return { ...initState }
  },
  getters: {
    doubleCount: (state) => state.count * 2
  },
  actions: {
    increment() {
      this.count++
    },
    async demoCallApi(){
      const res = await api.get<any>('personalInfo')
      console.log(res);
      
      return res
    }
  }
})

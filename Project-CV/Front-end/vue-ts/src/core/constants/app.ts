const BASE_URL = import.meta.env.VITE_APP_BASE_URL
const API_PREFIX = import.meta.env.VITE_APP_API_PREFIX

export const ENV_CONFIG = {
  API_URL: BASE_URL + '/' + API_PREFIX
}

export const DEFAULT_EMPTY = '-'

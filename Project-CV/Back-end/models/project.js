const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProjectSchema = new Schema({
    projectName: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    technologiesUsed: {
        type: [String]  // Mảng các công nghệ được sử dụng
    },
    link: {
        type: String  // URL hoặc đường dẫn đến dự án
    },
    // Các trường khác nếu cần
});

module.exports = mongoose.model('Project', ProjectSchema);

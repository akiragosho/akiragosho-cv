const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SkillSchema = new Schema({
    skillName: {
        type: String,
        required: true
    },
    proficiency: {
        type: String,  // Ví dụ: "Người mới bắt đầu", "Có kinh nghiệm", "Chuyên nghiệp", ...
        required: true
    },
    // Các trường khác nếu cần
});

module.exports = mongoose.model('Skill', SkillSchema);

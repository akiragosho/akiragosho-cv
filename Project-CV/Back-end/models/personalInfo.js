const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PersonalInfoSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String
    },
    address: {
        type: String
    },
    introduction: {
        type: String
    },
    profilePicture: {
        type: String, // Đây có thể là URL của hình ảnh
    },
    // Bạn có thể thêm nhiều trường khác nếu cần
});

module.exports = mongoose.model('PersonalInfo', PersonalInfoSchema);

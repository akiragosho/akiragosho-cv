const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CompanySchema = new Schema({
    companyName: {
        type: String,
        required: true
    },
    position: {
        type: String,
        required: true
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date
    },
    jobDescription: {
        type: String,
        required: true
    },
    // Các trường khác nếu cần
});

module.exports = mongoose.model('Company', CompanySchema);

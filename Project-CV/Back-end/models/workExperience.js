const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const WorkExperienceSchema = new Schema({
    position: {
        type: String,
        required: true
    },
    company: {
        type: String,
        required: true
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date
    },
    description: {
        type: String
    },
    // Các trường khác nếu cần
});

module.exports = mongoose.model('WorkExperience', WorkExperienceSchema);

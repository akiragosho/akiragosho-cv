const express = require("express");
const router = express.Router();
const WorkExperience = require("../models/workExperience");

router.get("/", async (req, res) => {
  try {
    const experiences = await WorkExperience.find();
    res.json({ status: 200, data: experiences });
  } catch (err) {
    res.status(500).json({ status: 500, data: { message: err.message } });
  }
});

// Thêm kinh nghiệm làm việc
router.post("/", async (req, res) => {
  const experience = new WorkExperience({
    // ... lấy dữ liệu từ req.body
  });

  try {
    const newExperience = await experience.save();
    res.status(201).json({ status: 201, data: newExperience });
  } catch (err) {
    res.status(400).json({ status: 400, data: { message: err.message } });
  }
});

module.exports = router;

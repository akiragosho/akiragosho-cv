const express = require("express");
const router = express.Router();
const PersonalInfo = require("../models/personalInfo");

// Lấy tất cả thông tin cá nhân
router.get("/", async (req, res) => {
  try {
    const personalInfo = await PersonalInfo.find();
    res.json({ status: 200, data: personalInfo });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Thêm thông tin cá nhân
router.post("/", async (req, res) => {
  const info = new PersonalInfo({
    name: req.body.name,
    email: req.body.email,
    phone: req.body.phone,
    address: req.body.address,
    introduction: req.body.introduction,
    profilePicture: req.body.profilePicture,
  });
  try {
    const newInfo = await info.save();
    res.status(201).json({ status: 201, data: newInfo });
  } catch (err) {
    res.status(400).json({ status: 400, data: { message: err.message } });
  }
});

module.exports = router;

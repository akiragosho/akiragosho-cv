const express = require('express');
const router = express.Router();
const Skill = require('../models/skill');

router.get('/', async (req, res) => {
    try {
        const skills = await Skill.find();
        res.json(skills);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.post('/', async (req, res) => {
    const skill = new Skill({
        // ... lấy dữ liệu từ req.body
    });
    try {
        const newSkill = await skill.save();
        res.status(201).json(newSkill);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

module.exports = router;

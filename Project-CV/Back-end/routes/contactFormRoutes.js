const express = require("express");
const router = express.Router();
const ContactForm = require("../models/contactForm");

// Lấy tất cả các form liên hệ đã gửi
router.get("/", async (req, res) => {
  try {
    const forms = await ContactForm.find();
    res.json(forms);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Gửi một form liên hệ mới
router.post("/", async (req, res) => {
  const form = new ContactForm({
    name: req.body.name,
    email: req.body.email,
    subject: req.body.subject,
    message: req.body.message
  });

  try {
    const newForm = await form.save();
    res.status(201).json(newForm);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

module.exports = router;

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Import routes
const personalInfoRoutes = require("./routes/personalInfoRoutes");
const workExperienceRoutes = require("./routes/workExperienceRoutes");
const projectRoutes = require("./routes/projectRoutes");
const skillRoutes = require("./routes/skillRoutes");
const companyRoutes = require("./routes/companiesRoutes");
const contactFormRoutes = require("./routes/contactFormRoutes");

// ... và các tệp routes khác

const app = express();
const username = encodeURIComponent("akiragosho");
const password = encodeURIComponent("7MiFQTnZcqWdkSht");

const uri = `mongodb+srv://${username}:${password}@cluster0.ydox5o3.mongodb.net/?retryWrites=true&w=majority`; // Thay <username> và <password>

mongoose
  .connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("Connected to MongoDB Atlas"))
  .catch((err) => console.error("Could not connect to MongoDB Atlas", err));

app.use(cors());
app.use(express.json());

// Sử dụng routes
app.use("/api/personalInfo", personalInfoRoutes);
app.use("/api/workExperience", workExperienceRoutes);
app.use("/api/projects", projectRoutes);
app.use("/api/skills", skillRoutes);
app.use("/api/companies", companyRoutes);
app.use("/api/contactForm", contactFormRoutes);

// ... và các tệp routes khác

const PORT = 5000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});